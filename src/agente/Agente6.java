package agente;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import java.util.Random;

public class Agente6 extends Agent{
	@Override
	protected void setup() {
		// TODO Auto-generated method stub
		addBehaviour(new Comportamiento());
		super.setup();
	}
	@Override
	protected void takeDown() {
		// TODO Auto-generated method stub
		System.out.println(getName()+" nooo");	
		super.takeDown();
	}
	
	class Comportamiento extends Behaviour{
		
		@Override
		public void action() {
			System.out.println(getName());	
			ACLMessage mensaje = blockingReceive();
			if(mensaje.getConversationId().equals("id36")){
                            System.out.println("Mensaje del Agente3 exitoso");
                        }else if(mensaje.getConversationId().equals("id16")){
                            System.out.println("Mensaje del Agente1 exitoso");
                        }
		}
                
                
    
			
			//Comunicacion
		 
		private void enviarMensaje(String contenido, String receptor, String idConversacion){
			AID id = new AID();
			id.setLocalName(receptor);
			ACLMessage acl = new ACLMessage(ACLMessage.INFORM);
			acl.addReceiver(id);
			acl.setSender(getAID());
			acl.setContent(contenido);
			acl.setLanguage("lol");
			acl.setConversationId(idConversacion);
			send(acl);
		}
		
	
		@Override
		public boolean done() {

			return false;
		}
		
	}
	
	
}
